import express from 'express';
import routes from './src/routes.mjs';

const app = express();
const port = 10050;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use((req, res, next) => {
  const now = new Date();
  const time = `${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
  console.log(`[${time}] ${req.method}\t${req.url}`, req.params);
  next();
});
app.use('/back/api', routes);

app.listen(port, () => {
  console.clear();
  console.log(`Server listening at http://localhost:${port}`);
});
