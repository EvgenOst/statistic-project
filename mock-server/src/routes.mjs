import express from 'express';
const router = express.Router();

import { createRandomRecords } from './data.mjs';

function responseData(response, data) {
  return response.status(200).json({
    errorCode: 0,
    errorMessage: '',
    count: data && data.length,
    data: data,
    success: true,
  });
}

function responseParams(response, data) {
  return response.status(200).json({
    errorCode: 0,
    errorMessage: '',
    success: true,
    ...data,
  });
}

function responseError(response, text, code) {
  return response.status(200).json({
    errorCode: code || 1,
    errorMessage: text || '',
    success: false,
  });
}

function getDictionary(table) {
  return dictionaries[table] || [{ tid: -1, Name: 'Не найдено' }];
}

const dictionaries = {
  States: [
    { tid: 1, Name: 'New' },
    { tid: 2, Name: 'Active' },
    { tid: 3, Name: 'Pause' },
    { tid: 4, Name: 'Closed' },
  ],
  Sprints: [
    { tid: 1, Name: '1.1' },
    { tid: 2, Name: '1.2' },
    { tid: 3, Name: '1.3' },
    { tid: 4, Name: '1.4' },
  ],
  TaskTypes: [
    { tid: 1, Name: 'Feature' },
    { tid: 2, Name: 'Story' },
    { tid: 3, Name: 'Task' },
    { tid: 4, Name: 'Bug' },
  ],
};
const defaultTaskFields = [
  { name: 'tid', type: 'id' },
  { name: 'Name', type: 'string' },
  { name: 'State', type: 'list', values: dictionaries.States },
  { name: 'Sprint', type: 'list', values: dictionaries.Sprints },
  { name: 'CreateDate', type: 'date' },
  { name: 'CloseDate', type: 'date' },
  { name: 'Estimate', type: 'number' },
  { name: 'Completed', type: 'number' },
  { name: 'CompletePersent', type: 'number' },
];
const treeTaskFields = [
  ...defaultTaskFields,
  {
    name: 'Items',
    type: 'child',
    fields: [
      ...defaultTaskFields,
      { name: 'Items', type: 'child', fields: [...defaultTaskFields] },
    ],
  },
];

let taskTree = createRandomRecords(treeTaskFields, 100);
router.all('/', (req, res) => {
  const source = req.query.SourceID || req.body.SourceID;
  const tableName = req.query.TableName || req.body.TableName;

  switch (source) {
    case 'Auth':
      return responseParams(res, { SessionGUID: '0000-0000-0000-0000' });
    case 'CheckSession':
      return responseParams(res, { user_id: 1 });
    case 'Dictionary_GetAll':
      return responseData(res, getDictionary(tableName));
    case 'TaskTree_GetAll':
      return responseData(res, taskTree);
    case 'Task_Get':
      return responseData(res, taskTree);

    default:
      console.log('[DEBUG]', req);
      return responseError(res, `SourceID ${source} не найден`);
  }
});

// router.all('/Auth', (req, res) => responseParams(res, { SessionGUID: '0000-0000-0000-0000' }));
// router.get('/UserInfo', (req, res) => responseParams(res, { User: { Name: 'Hasalam' } }));
// router.get('/tasks/tree', (req, res) => responseData(res, taskTree));
// router.get('/combo/ItemTypes', (req, res) => responseData(res, types));
// router.get('/combo/ItemStates', (req, res) => responseData(res, states));

export default router;
