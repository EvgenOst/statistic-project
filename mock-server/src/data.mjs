const db = {
  // prettier-ignore
  tasks: [
    {tid: 1, Parent_id: null, Title: 'TMS Feature', ItemType_id: 1, name_ItemType_id: 'Feature', State_id: 1, name_State_id: 'New', CreateDate: ISODate('2021-09-14') },
    {tid: 2, Parent_id: 1, Title: 'TMS Story 1', ItemType_id: 2, name_ItemType_id: 'Story', State_id: 1, name_State_id: 'New', CreateDate: ISODate('2021-09-14') },
    {tid: 3, Parent_id: 1, Title: 'TMS Story 2', ItemType_id: 2, name_ItemType_id: 'Story', State_id: 1, name_State_id: 'New', CreateDate: ISODate('2021-09-15') },
    {tid: 4, Parent_id: 3, Title: 'TMS TASK', ItemType_id: 3, name_ItemType_id: 'Task', State_id: 1, name_State_id: 'New', CreateDate: ISODate('2021-09-11') },
    {tid: 5, Parent_id: null, Title: 'TMS Feature 2', ItemType_id: 1, name_ItemType_id: 'Feature', State_id: 1, name_State_id: 'New', CreateDate: ISODate('2021-01-14') },
  ],
};

function ISODate(string) {
  return (string ? new Date(string) : new Date()).toISOString();
}

function randomNumber(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function randomString(len) {
  let str = '';
  for (let i = len; i--; ) {
    let c = randomNumber(10, 62);
    str += String.fromCharCode((c += c > 9 ? (c < 36 ? 55 : 61) : 48));
  }
  return str;
}

function randomText(length) {
  var result = '';
  var characters = '     ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

const nullFn = () => null;

function createModelFactory(typeOptions) {
  return (fields) => (data = {}) =>
    fields.reduce(
      (acc, field) =>
        (acc[field.name] = (typeOptions[field.type] || nullFn)(data[field.name], field), acc),
      {}
    );
}

function createRandomRecords(fields, count) {
  let genId = 0;

  const RandomModelFactory = createModelFactory({
    id: () => ++genId,
    number: () => randomNumber(0, 1000),
    string: () => randomText(randomNumber(8, 30)),
    date: () => ISODate(),
    list: (v, f) => f.values[randomNumber(0, f.values.length)],
    child: (v, f) => new Array(randomNumber(0, 5)).fill({}).map(RandomModelFactory(f.fields || []))
  });

  const Model = RandomModelFactory(fields);
  const result = [];
  for (let i = count; i--; ) {
    result.push(Model());
  }

  return result;
}

function getDictionaryTable(name) {
  return db.dictionary[name];
}

function getTable(name) {
  return db[name];
}

export { createRandomRecords, getDictionaryTable, getTable };
