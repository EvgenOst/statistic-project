import React, { useEffect } from 'react';
import { HashRouter as Router } from 'react-router-dom';
import LoadPanel from 'devextreme-react/load-panel';
import { loadMessages, locale } from 'devextreme/localization';
import ruMessages from 'devextreme/localization/messages/ru.json';
import { Content } from './Content';
import { NavigationProvider } from './contexts/navigation';
import { UnauthenticatedContent } from './UnauthenticatedContent';
import { useScreenSizeClass } from './utils/media-query';
import 'devextreme/dist/css/dx.common.css';
import './themes/generated/theme.additional.css';
import './themes/generated/theme.base.css';
import './dx-styles.scss';
import { StoresContext, initContextsValues } from './contexts/stores';
import { observer } from 'mobx-react-lite';

const storeContext = initContextsValues();

const AppContent = observer(() => {
  const authStore = storeContext.authStore;

  if (!authStore.initialized) {
    return <LoadPanel visible={true} />;
  }

  if (authStore.authorized) {
    return <Content />;
  }

  return <UnauthenticatedContent />;
});

export function App(): JSX.Element {
  const screenSizeClass = useScreenSizeClass();

  console.log('App');

  useEffect(() => {
    storeContext.authStore.init();
  }, []);

  loadMessages(ruMessages);
  locale('ru');

  return (
    <Router>
      <NavigationProvider>
        <StoresContext.Provider value={storeContext}>
          <div className={`app ${screenSizeClass}`}>
            <AppContent />
          </div>
        </StoresContext.Provider>
      </NavigationProvider>
    </Router>
  );
}
