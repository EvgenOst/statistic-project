import { IColumnLookupProps } from 'devextreme-react/tree-list';
import { CustomStoreOptions } from 'devextreme/data/custom_store';
import DataSource, { DataSourceOptions } from 'devextreme/data/data_source';
import { IBaseApi } from '../api';
import { SourceDataParams } from '../services';

export interface ILookupOptions {
  valueExpr?: string;
  displayExpr?: string;
}

export interface IStoreOptions {
  api: IBaseApi;
  cache?: boolean;
  keyField?: string;
  parentKey?: string;
}

export function createDataSourceCfg(
  options: IStoreOptions,
  sourceOptions?: DataSourceOptions
): DataSourceOptions {
  return {
    ...createStoreCfg(options),
    ...sourceOptions,
  };
}

export function createDataSource(
  options: IStoreOptions,
  sourceOptions?: DataSourceOptions
): DataSource {
  return new DataSource({
    ...createStoreCfg(options),
    ...sourceOptions,
  });
}

export function createLookupCfg(api: IBaseApi, options?: ILookupOptions): IColumnLookupProps {
  const valueField = (options && options.valueExpr) || 'tid';
  const displayField = (options && options.displayExpr) || 'Name';

  return {
    dataSource: createStoreCfg({ api: api, keyField: valueField }),
    valueExpr: valueField,
    displayExpr: displayField,
  };
}

export function createStoreCfg(options: IStoreOptions): CustomStoreOptions {
  const { api } = options;
  const keyField = options.keyField || 'tid';

  const storeOpts: CustomStoreOptions = {
    cacheRawData: options.cache,
    key: keyField,
  };

  if (api.getAll) {
    storeOpts.load = async function (loadOptions) {
      const params: SourceDataParams = {
        start: loadOptions.skip || 0,
        limit: loadOptions.take || 30,
        ...(loadOptions as any).extraParams,
      };

      console.log('load', { loadOptions });
      if (loadOptions.searchOperation === 'contains' && loadOptions.searchValue) {
        params.query = loadOptions.searchValue;
      }

      if (
        loadOptions.filter &&
        loadOptions.filter.every((v: any) => ['number', 'string'].includes(typeof v))
      ) {
        params.filter = JSON.stringify([
          { property: loadOptions.filter[0], operator: 'eq', value: loadOptions.filter[2] },
        ]);
      }

      // @ts-expect-error Check getAll before define fn
      const response = await api.getAll(params);
      return { data: response.data, totalCount: response.totalCount };
    };
  }

  if (api.getOne) {
    storeOpts.byKey = async function (key: number) {
      // @ts-expect-error Check getOne before define fn
      return (await api.getOne(key)).data?.[0];
    };
  }

  if (api.post) {
    storeOpts.insert = async function (values: any) {
      // @ts-expect-error Check post before define fn
      await api.post(values);
    };
    storeOpts.update = async function (key: number, values: any) {
      // @ts-expect-error Check post before define fn
      await api.post({ [keyField]: key, ...values });
    };
  }

  return storeOpts;
}
