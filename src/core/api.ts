import { getSourceData, runAction, SourceDataParams } from './services';
import { IResponseData } from './services/request';

type ActionResponse = Promise<IResponseData>;
export interface IBaseApi<T = unknown> {
  readonly getAll?: (params: SourceDataParams) => ActionResponse;
  readonly getOne?: (id: number) => ActionResponse;
  readonly post?: (data: T) => ActionResponse;
  readonly delete?: (id: number) => ActionResponse;
}

export interface IAuthResponse extends IResponseData {
  SessionGUID: string;
}

export interface IUserInfoResponse extends IResponseData {
  user_id: string;
}

export const Auth = {
  signIn: (username: string, password: string): Promise<IAuthResponse> =>
    runAction('Auth', { UserName: username, UserPassword: password }) as Promise<IAuthResponse>,
  checkSession: (): Promise<IUserInfoResponse> =>
    runAction('CheckSession') as Promise<IUserInfoResponse>,
};

export function createDictionaryApi(table: string, schema = 'dbo', isShort = true): IBaseApi {
  return {
    getAll: (params) =>
      getSourceData('GetDictionary', {
        tableName: table,
        isShort: isShort,
        schemaName: schema,
        ...params,
      }),
  };
}

export const Project: IBaseApi = {
  getOne: (id) => runAction('TTS_ProjectGet', { Project_id: id }),
  post: (data) => runAction('TTS_ProjectSet', data),
  delete: (id) => runAction('TTS_ProjectDelete', { Project_id: id }),
};

export const Idea = {
  getOne: (id: number): ActionResponse => runAction('TTS_IdeaGet', { Idea_id: id }),
  post: (data: any): ActionResponse => runAction('TTS_IdeaSet', data),
  delete: (id: number): ActionResponse => runAction('TTS_IdeaDelete', { Idea_id: id }),
  setToSprint: (id: number, sprintID: number): ActionResponse =>
    runAction('TTS_IdeaToSprintSet', { Idea_id: id, Sprint_id: sprintID }),
};

export const Task = {
  getOne: (id: number): ActionResponse => runAction('TTS_TaskGet', { Task_id: id }),
  post: (data: any): ActionResponse => runAction('TTS_TaskSet', data),
  delete: (id: number): ActionResponse => runAction('TTS_TaskDelete', { Task_id: id }),
  setToSprint: (id: number, sprintID: number): ActionResponse =>
    runAction('TTS_TaskToSprintSet', { Task_id: id, Sprint_id: sprintID }),
};

export const BacklogTree: IBaseApi = {
  getAll: (p) => getSourceData('TTS_GetBackLogTree', p),
};

export const Sprint: IBaseApi = {
  getAll: (p) => getSourceData('TTS_GetSprints', p),
};

export function createSprintPeoplesApi(sprintID: number): IBaseApi {
  return {
    getAll: (p) => getSourceData('TTS_GetSprintPeople', { Sprint_id: sprintID, ...p }),
  };
}
