export type UrlParams = (string | number)[] | { [key: string]: boolean | number | string };
export type Params = { [key: string]: boolean | number | string };
export type Headers = { [key: string]: string };

export type Method = 'GET' | 'POST' | 'PUT' | 'DELETE';
export type BodyType = 'JSON' | 'FORM';

export interface IRequestOptions {
  url: string;
  method?: Method;
  headers?: Headers;
  urlParams?: UrlParams;
  queryParams?: Params;
  bodyParams?: Params;
}

export interface IResponseData<T = unknown> {
  readonly data: T[];
  readonly ErrorCode: number;
  readonly ErrorMessage: string;
  readonly success: boolean;
  readonly totalCount: number;
}

export class ResponseError extends Error {
  constructor(public message: string, public code: number) {
    super(message);
  }
}

class Request {
  async get<T>(opts: IRequestOptions): Promise<IResponseData<T>> {
    opts.method = 'GET';
    return this.send<T>(opts);
  }

  async post<T>(opts: IRequestOptions): Promise<IResponseData<T>> {
    opts.method = 'POST';
    return this.send<T>(opts);
  }

  async delete(opts: IRequestOptions): Promise<IResponseData> {
    opts.method = 'DELETE';
    return this.send(opts);
  }

  async send<T>(opts: IRequestOptions): Promise<IResponseData<T>> {
    let url = opts.url;
    let body;

    opts.headers = {
      ...opts.headers,
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    };

    if (opts.urlParams) {
      url = this.buildUrlWithParams(url, opts.urlParams);
    }

    if (opts.queryParams) {
      url += '?' + this.buildQueryParams(opts.queryParams);
    }

    if (opts.bodyParams) {
      body = this.buildQueryParams(opts.bodyParams);
    }

    const response = await fetch(url, {
      method: opts.method,
      headers: opts.headers,
      body: body,
      mode: 'same-origin',
    });

    return this.processResponse<T>(response);
  }

  buildUrlWithParams(base: string, params: UrlParams): string {
    let replacer;
    if (Array.isArray(params)) {
      let i = 0;
      replacer = () => String(params[i++]);
    } else {
      replacer = (m: string, p1: string) => String(params[p1]);
    }

    return base.replace(/{(\w*)}/g, replacer);
  }

  buildQueryParams(params: Params, ignore?: RegExp): string {
    const result: [string, string][] = [];
    for (const [key, value] of Object.entries(params)) {
      if (ignore && ignore.test(key)) continue;

      result.push([key, String(value)]);
    }

    return new URLSearchParams(result).toString();
  }

  buildBodyParams(params: Params, ignore?: RegExp): FormData {
    const data = new FormData();

    for (const [key, value] of Object.entries(params)) {
      if (ignore && ignore.test(key)) continue;

      data.append(key, String(value));
    }

    return data;
  }

  async processResponse<T>(response: Response): Promise<IResponseData<T>> {
    let result = null;

    if (!response.ok) {
      if (response.status === 401) {
        this.raiseResponseError({ message: 'Ошибка авторизации', code: -1, response });
      }

      this.raiseResponseError({ message: response.statusText, code: response.status, response });
    }

    try {
      result = (await response.json()) as IResponseData<T>;
    } catch (e) {
      this.raiseResponseError({
        message: 'Ошибка во время разбора ответа',
        response,
      });
    }

    if (!result) {
      this.raiseResponseError({ message: 'Сервер вернул пустой ответ', response });
    }

    if (!result.success) {
      this.raiseResponseError({ message: result.ErrorMessage, code: result.ErrorCode, response });
    }

    return result;
  }

  private raiseResponseError(opts: { code?: number; message: string; response: Response }): never {
    const { code = 0, message, response } = opts;
    // prettier-ignore
    console.error(`[${new Date().toLocaleString()}][Request] ${response.url}\n(${code}) ${message}`);
    throw new ResponseError(message, code);
  }
}

export default new Request();
