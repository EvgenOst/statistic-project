import { IResponseData } from '.';
import { API_URL } from '../../const/url';
import Request, { Params } from './request';

interface ISourceParams {
  start: number;
  limit: number;
  // filter?: string;
  // sort?: string;
  // query?: string;
}

export type SourceDataParams = Params & ISourceParams;

export function runAction<T, R = undefined>(source: string, data?: T): Promise<IResponseData<R>> {
  return Request.post({ url: API_URL, bodyParams: { sourceID: source, ...data } });
}

export function getSourceData(source: string, params: SourceDataParams): Promise<IResponseData> {
  return Request.get({ url: API_URL, queryParams: { sourceID: source, ...params } });
}
