import { Nullable } from '../core/types/nullable';

export interface Project {
  tid: number;
  Name: Nullable<string>;
  Description: Nullable<string>;
  DateStart: Nullable<Date>;
  DateFinish: Nullable<Date>;

  State_id: Nullable<number>;
  name_State_id: Nullable<string>;

  Sprint_id: Nullable<number>;
  name_Sprint_id: Nullable<string>;

  People_id: Nullable<number>;
  name_People_id: Nullable<string>;
}
