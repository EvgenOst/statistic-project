import { Nullable } from '../core/types/nullable';

export interface Task {
  tid: number;
  Name: Nullable<string>;
  Description: Nullable<string>;
  Priority: Nullable<number>;
  DateStart: Nullable<Date>;
  DateFinish: Nullable<Date>;
  EstimatedTime: Nullable<number>;

  State_id: Nullable<number>;
  name_State_id: Nullable<string>;

  Sprint_id: Nullable<number>;
  name_Sprint_id: Nullable<string>;

  People_id: Nullable<number>;
  name_People_id: Nullable<string>;

  Project_id: Nullable<number>;
  name_Project_id: Nullable<string>;

  Idea_id: Nullable<number>;
  name_Idea_id: Nullable<string>;

  ParentTask_id: Nullable<number>;
  name_ParentTask_id: Nullable<string>;
}
