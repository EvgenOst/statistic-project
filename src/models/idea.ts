import { Nullable } from '../core/types/nullable';

export interface Idea {
  tid: number;
  Name: Nullable<string>;
  Description: Nullable<string>;
  Priority: Nullable<number>;
  DateStart: Nullable<Date>;
  DateFinish: Nullable<Date>;

  Modules: Nullable<string>;

  State_id: Nullable<number>;
  name_State_id: Nullable<string>;

  Sprint_id: Nullable<number>;
  name_Sprint_id: Nullable<string>;

  People_id: Nullable<number>;
  name_People_id: Nullable<string>;

  Release_id: Nullable<number>;
  name_Release_id: Nullable<string>;

  Project_id: Nullable<number>;
  name_Project_id: Nullable<string>;

  ParentIdea_id: Nullable<number>;
  name_ParentIdea_id: Nullable<string>;
}
