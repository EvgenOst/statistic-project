import React, { useState, useRef, useCallback, useContext } from 'react';
import Form, { Item, Label, ButtonItem, ButtonOptions, RequiredRule } from 'devextreme-react/form';
import LoadIndicator from 'devextreme-react/load-indicator';

import './login-form.scss';
import { StoresContext } from '../../contexts/stores';

const emailEditorOptions = { stylingMode: 'filled', placeholder: 'Login' };
const passwordEditorOptions = { stylingMode: 'filled', placeholder: 'Password', mode: 'password' };
const rememberMeEditorOptions = { text: 'Remember me', elementAttr: { class: 'form-text' } };

export function LoginForm(): JSX.Element {
  const stores = useContext(StoresContext);
  const [loading, setLoading] = useState(false);
  const formData = useRef<any>({});

  const onSubmit = useCallback(
    async (e) => {
      e.preventDefault();
      const { login, password } = formData.current;

      setLoading(true);
      const successful = await stores.authStore.signIn(login, password);
      if (!successful) {
        setLoading(false);
      }
    },
    [stores]
  );

  return (
    <form className={'login-form'} onSubmit={onSubmit}>
      <Form formData={formData.current} disabled={loading}>
        <Item dataField={'login'} editorType={'dxTextBox'} editorOptions={emailEditorOptions}>
          <RequiredRule message="Login is required" />
          <Label visible={false} />
        </Item>
        <Item dataField={'password'} editorType={'dxTextBox'} editorOptions={passwordEditorOptions}>
          <RequiredRule message="Password is required" />
          <Label visible={false} />
        </Item>
        <Item
          dataField={'rememberMe'}
          editorType={'dxCheckBox'}
          editorOptions={rememberMeEditorOptions}
        >
          <Label visible={false} />
        </Item>
        <ButtonItem>
          <ButtonOptions width={'100%'} type={'default'} useSubmitBehavior={true}>
            <span className="dx-button-text">
              {loading ? <LoadIndicator width={'24px'} height={'24px'} visible={true} /> : 'Вход'}
            </span>
          </ButtonOptions>
        </ButtonItem>
      </Form>
    </form>
  );
}
