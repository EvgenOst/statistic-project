import React from 'react';
import SplitterLayout, { SplitterProps } from 'm-react-splitters';
import 'm-react-splitters/lib/splitters.css';

export function Splitter(props: SplitterProps): JSX.Element {
  return <SplitterLayout postPoned={true} {...props} />;
}
