import React, { FC, useCallback } from 'react';
import { observer } from 'mobx-react-lite';
import { Button, Form, Popup } from 'devextreme-react';
import { Item } from 'devextreme-react/form';
import { RootStoreInstance } from '../../stores';

export const ProjectCard: FC = observer(() => {
  const store = RootStoreInstance.projectCardStore;

  const handleSubmit = useCallback(
    (e) => {
      e.preventDefault();
      store.submitAndClose();
    },
    [store]
  );

  return (
    <Popup
      showCloseButton={true}
      height={'auto'}
      defaultWidth={800}
      visible={store.visible}
      onHiding={store.close}
      resizeEnabled={true}
      closeOnOutsideClick={true}
      title={store.data ? 'Проект #' + store.data.tid : 'Добавление проекта'}
    >
      <form onSubmit={handleSubmit}>
        <Form formData={store.data} colCount={1} labelLocation={'top'} scrollingEnabled={true}>
          <Item itemType="group" colCount={12}>
            <Item
              dataField={'Name'}
              editorType={'dxTextBox'}
              colSpan={12}
              label={{ visible: false }}
            />
          </Item>
          <Item itemType="group" colCount={3}>
            <Item itemType="group">
              <Item dataField={'State_id'} label={{ text: 'Статус' }} editorType={'dxTextBox'} />
              <Item
                dataField={'People_id'}
                label={{ text: 'Ответственный' }}
                editorType={'dxTextBox'}
              />
              <Item
                dataField={'DateStart'}
                label={{ text: 'Дата начала' }}
                editorType={'dxDateBox'}
              />
              <Item
                dataField={'DateFinish'}
                label={{ text: 'Дата завершения' }}
                editorType={'dxDateBox'}
              />
            </Item>
            <Item itemType="group" caption={'Описание'} colCount={1} colSpan={2}>
              <Item
                dataField={'Description'}
                editorType={'dxTextArea'}
                label={{ visible: false }}
                editorOptions={{ autoResizeEnabled: true, maxHeight: 260 }}
              />
            </Item>
          </Item>
        </Form>
        <div className="button-group">
          <Button text={'Отмена'} onClick={store.close} />
          <Button text={'Сохранить'} useSubmitBehavior={true} />
        </div>
      </form>
    </Popup>
  );
});
