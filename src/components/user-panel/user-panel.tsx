import React, { useContext, useMemo } from 'react';
import { useHistory } from 'react-router-dom';
import ContextMenu, { Position } from 'devextreme-react/context-menu';
import List from 'devextreme-react/list';
import './user-panel.scss';
import { useCallback } from 'react';
import { StoresContext } from '../../contexts/stores';

interface IUserPanelProps {
  menuMode: string;
}

export function UserPanel({ menuMode }: IUserPanelProps): JSX.Element {
  const { authStore } = useContext(StoresContext);
  const history = useHistory();

  const navigateToProfile = useCallback(() => {
    history.push('/profile');
  }, [history]);

  const menuItems = useMemo(
    () => [
      {
        text: 'Profile',
        icon: 'user',
        onClick: navigateToProfile,
      },
      {
        text: 'Logout',
        icon: 'runner',
        onClick: authStore.signOut,
      },
    ],
    [authStore, navigateToProfile]
  );

  return (
    <div className={'user-panel'}>
      <div className={'user-info'}>
        <div className={'image-container'}>
          <div
            style={{
              background: `url(${authStore.userData.avatarUrl}) no-repeat #fff`,
              backgroundSize: 'cover',
            }}
            className={'user-image'}
          />
        </div>
        <div className={'user-name'}>{authStore.userData.email}</div>
      </div>

      {menuMode === 'context' && (
        <ContextMenu
          items={menuItems}
          target={'.user-button'}
          showEvent={'dxclick'}
          width={210}
          cssClass={'user-menu'}
        >
          <Position my={'top center' as any} at={'bottom center' as any} />
        </ContextMenu>
      )}
      {menuMode === 'list' && <List className={'dx-toolbar-menu-action'} items={menuItems} />}
    </div>
  );
}
