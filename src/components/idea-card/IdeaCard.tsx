import React, { FC, useCallback, useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { Button, Form, Popup } from 'devextreme-react';
import { Item } from 'devextreme-react/form';
import { RootStoreInstance } from '../../stores';

export const IdeaCard: FC = observer(() => {
  const store = RootStoreInstance.ideaCardStore;

  const handleSubmit = useCallback(
    (e) => {
      e.preventDefault();
      store.submitAndClose();
    },
    [store]
  );

  return (
    <Popup
      showCloseButton={true}
      height={'auto'}
      defaultWidth={800}
      visible={store.visible}
      onHiding={store.close}
      resizeEnabled={true}
      closeOnOutsideClick={true}
      title={store.data ? 'Идея #' + store.data.tid : 'Добавление идеи'}
    >
      <form onSubmit={handleSubmit}>
        <Form formData={store.data} colCount={1} labelLocation={'top'} scrollingEnabled={true}>
          <Item itemType="group" colCount={12}>
            <Item
              dataField={'Name'}
              editorType={'dxTextBox'}
              colSpan={12}
              label={{ visible: false }}
            />
          </Item>
          <Item itemType="group" colCount={3}>
            <Item itemType="group">
              <Item dataField={'State_id'} label={{ text: 'Статус' }} editorType={'dxTextBox'} />
              <Item
                dataField={'People_id'}
                label={{ text: 'Ответственный' }}
                editorType={'dxTextBox'}
              />
              <Item
                dataField={'Priority'}
                label={{ text: 'Приоритет' }}
                editorType={'dxNumberBox'}
              />
              <Item dataField={'Sprint_id'} label={{ text: 'Спринт' }} editorType={'dxTextBox'} />
              <Item
                dataField={'DateStart'}
                label={{ text: 'Дата начала' }}
                editorType={'dxDateBox'}
              />
              <Item
                dataField={'DateFinish'}
                label={{ text: 'Дата завершения' }}
                editorType={'dxDateBox'}
              />
              <Item dataField={'Modules'} label={{ text: 'Модули' }} editorType={'dxTextBox'} />
              <Item dataField={'Release_id'} label={{ text: 'Релиз' }} editorType={'dxTextBox'} />
            </Item>
            <Item itemType="group" caption={'Описание'} colCount={1} colSpan={2}>
              <Item
                dataField={'Description'}
                editorType={'dxTextArea'}
                label={{ visible: false }}
                editorOptions={{ autoResizeEnabled: true, maxHeight: 260 }}
              />
            </Item>
          </Item>
        </Form>
        <div className="button-group">
          <Button text={'Отмена'} onClick={store.close} />
          <Button text={'Сохранить'} useSubmitBehavior={true} />
        </div>
      </form>
    </Popup>
  );
});
