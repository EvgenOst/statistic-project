import {
  ColumnChooser,
  ColumnFixing,
  DataGrid as Grid,
  Editing,
  FilterRow,
  Pager,
  Paging,
  Popup,
  Selection,
} from 'devextreme-react/data-grid';
import DataSource from 'devextreme/data/data_source';
import dxDataGrid, { ColumnBase, ToolbarPreparingEvent } from 'devextreme/ui/data_grid';
import React, { RefObject } from 'react';
import { createLookupCfg, ILookupOptions } from '../../core/utils/data-source-utils';
import './data-grid.scss';

export interface IColumnConfig extends Omit<ColumnBase, 'lookup'> {
  lookup?: ILookupOptions | string;
}

export interface IDataGridProps {
  children?: any[];
  className?: string;
  columns?: IColumnConfig[];
  dataSource: any;
  disabled?: boolean;
  editPopupTemplate?: any;
  editable?: boolean;
  enableColumnFixing?: boolean;
  masterRow?: unknown | null;
  tbar?: unknown[];
  title?: string;
  onSelectionChange?: (selection: unknown, selected: unknown[]) => void;
  onSelectionKeyChange?: (selection: string | number, selected: (string | number)[]) => void;
}

export class DataGrid extends React.Component<IDataGridProps, any> {
  static defaultProps = {
    disabled: false,
    editable: true,
    enableColumnFixing: true,
  };

  public selection: unknown;

  private _dataGridRef: RefObject<Grid>;
  private _columns?: ColumnBase[];
  private _dataSource: DataSource;

  constructor(props: IDataGridProps) {
    super(props);

    console.log('[DataGrid] constructor', props);

    this._dataGridRef = React.createRef();
    this.onRowDblClick = this.onRowDblClick.bind(this);
    this.onSelectionChanged = this.onSelectionChanged.bind(this);
    this.onToolbarPreparing = this.onToolbarPreparing.bind(this);
    this._dataSource = this.props.dataSource;
  }

  componentDidMount(): void {
    if (this.props.masterRow) {
      this.updateMasterSourceParam(this.props.masterRow);
    }
  }

  shouldComponentUpdate(nextProps: IDataGridProps): boolean {
    const ignoredKey = ['columns', 'children', 'editPopupTemplate'];

    let key: keyof IDataGridProps;
    for (key in nextProps) {
      if (ignoredKey.includes(key)) continue;
      if (nextProps[key] !== this.props[key]) return true;
    }

    return false;
  }

  componentDidUpdate(prevProps: IDataGridProps): void {
    const changes: any = {};
    const props: any = this.props;
    const prev: any = prevProps;
    for (const key in props) {
      changes[key] = [prev[key] !== props[key], prev[key], props[key]];
    }
    console.log('[DataGrid] componentDidUpdate', changes);

    if (prevProps.masterRow !== this.props.masterRow && this.props.masterRow) {
      this.updateMasterSourceParam(this.props.masterRow);
    }
  }

  componentWillUnmount(): void {
    console.log('componentWillUnmount');

    this.getDataSource().dispose();
  }

  public get instance(): dxDataGrid | undefined {
    return this._dataGridRef.current?.instance;
  }

  public refresh(): void {
    this.instance?.refresh();
  }

  private getDataSource(): DataSource {
    if (!this._dataSource) {
      // this._dataSource = createDataSource(this.props.dataSource);
    }
    return this._dataSource;
  }

  private getColumns() {
    if (!this._columns && this.props.columns) {
      this._columns = this.props.columns.map((c) => {
        const out: any = { ...c };
        if (out.lookup) {
          out.lookup = createLookupCfg(out.lookup);
        }
        return out;
      });
      console.log('[DataGrid] processColumns', this._columns);
    }

    return this._columns;
  }

  private updateMasterSourceParam(masterRow: any): void {
    const source = this.getDataSource();

    source.loadOptions().parentIds = [masterRow];
    source.reload();
  }

  private onRowClick(e: any) {
    // console.log('on row click', e);
  }

  private onRowDblClick({ rowIndex }: any): void {
    if (this.props.editable) {
      this.instance?.editRow(rowIndex);
    }
  }

  private onSelectionChanged({ selectedRowKeys, selectedRowsData }: any): void {
    const { onSelectionChange, onSelectionKeyChange } = this.props;
    const selection = selectedRowsData[selectedRowsData.length - 1] || null;
    const selectionKey = selectedRowKeys[selectedRowKeys.length - 1] || null;

    if (onSelectionChange) onSelectionChange(selection, selectedRowsData);
    if (onSelectionKeyChange) onSelectionKeyChange(selectionKey, selectedRowKeys);
  }

  private onToolbarPreparing({ toolbarOptions }: ToolbarPreparingEvent): void {
    const topButtons = this.props.tbar;
    const items = toolbarOptions.items;

    if (items) {
      for (const item of items) {
        if (item.name === 'addRowButton') {
          item.location = 'before';
        }
      }

      toolbarOptions.items = items.concat(this.getDefaultsToolbarButtons());

      if (topButtons) {
        toolbarOptions.items = items.concat(topButtons);
      }
    }
  }

  private getDefaultsToolbarButtons(): any[] {
    return [
      {
        widget: 'dxButton',
        location: 'after',
        options: { icon: 'refresh', hint: 'Обновить', onClick: this.refresh.bind(this) },
      },
    ];
  }

  render(): JSX.Element {
    const props = this.props;
    const disabled = props.masterRow === null || props.disabled;

    const generated: any = {};
    const dataSource = this.getDataSource();
    const columns = this.getColumns();
    if (dataSource) generated.dataSource = dataSource;
    if (columns) generated.columns = columns;

    console.log('[DataGrid] render', props);

    return (
      <div className={'lgx-grid-wrapper ' + (props.className || '')}>
        {props.title && <div className="lgx-grid-title">{props.title}</div>}
        <Grid
          ref={this._dataGridRef}
          allowColumnReordering={true}
          allowColumnResizing={true}
          columnAutoWidth={true}
          columnHidingEnabled={true}
          columnResizingMode={'widget'}
          disabled={disabled}
          focusedRowEnabled={true}
          height={'100%'}
          remoteOperations={true}
          repaintChangesOnly={true}
          rowAlternationEnabled={true}
          showBorders={true}
          showColumnLines={true}
          showRowLines={true}
          onRowClick={this.onRowClick}
          onRowDblClick={this.onRowDblClick}
          onSelectionChanged={this.onSelectionChanged}
          onToolbarPreparing={this.onToolbarPreparing}
          {...generated}
        >
          <FilterRow visible={true} />
          <ColumnChooser enabled={true} mode={'select'} />
          <ColumnFixing enabled={props.enableColumnFixing} />
          <Paging defaultPageSize={30} />
          <Selection
            allowSelectAll={true}
            mode={'multiple'}
            selectAllMode={'page'}
            showCheckBoxesMode={'onLongTap'}
          />
          <Pager
            displayMode={'adaptive'}
            showPageSizeSelector={true}
            showInfo={true}
            allowedPageSizes={[30, 50, 100]}
          />
          {props.editable && (
            <Editing
              allowAdding={true}
              allowDeleting={true}
              allowUpdating={true}
              mode="popup"
              useIcons={true}
            >
              <Popup
                animation={undefined}
                dragEnabled={true}
                resizeEnabled={true}
                showCloseButton={true}
                showTitle={true}
                title={'Форма редактирования'}
              />
              {props.editPopupTemplate}
            </Editing>
          )}
          {props.children}
        </Grid>
      </div>
    );
  }
}
