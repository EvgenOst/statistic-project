import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { appInfo } from './app-info';
import routes from './app-routes';
import { IdeaCard, ProjectCard, TaskCard } from './components';
import { SideNavOuterToolbar as SideNavBarLayout } from './layouts';
// import { Footer } from "./components";

export function Content(): JSX.Element {
  return (
    <SideNavBarLayout title={appInfo.title}>
      <ProjectCard />
      <IdeaCard />
      <TaskCard />
      <Switch>
        {routes.map(({ path, component }) => (
          <Route exact key={path} path={path} component={component} />
        ))}
        <Redirect to={'/home'} />
      </Switch>
    </SideNavBarLayout>
  );
}
