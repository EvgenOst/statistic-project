import React, { Context, createContext, FC } from 'react';
import { RootStoreInstance } from '../../stores';
import { BacklogStore } from './backlog-store';
import { Tree } from './views/Tree';
import { WorkDetail } from './views/WorkDetail';
import './BacklogPage.scss';

const backlogContextValue = {
  backlogStore: new BacklogStore(RootStoreInstance),
};

interface IBacklogContext {
  backlogStore: BacklogStore;
}
export const BacklogContext = createContext<IBacklogContext | null>(
  null
) as Context<IBacklogContext>;

export const BacklogPage: FC = () => {
  return (
    <BacklogContext.Provider value={backlogContextValue}>
      <div className={'layout-vertical layout-stretch'}>
        <div className={'page-title'}>Backlog</div>
        <div className={'content-block flex-1 layout-horizontal'}>
          <Tree className={'flex-1'} />
          <WorkDetail />
        </div>
      </div>
    </BacklogContext.Provider>
  );
};
