import DataSource from 'devextreme/data/data_source';
import { confirm } from 'devextreme/ui/dialog';
import notify from 'devextreme/ui/notify';
import { action, makeObservable, observable } from 'mobx';
import { WORK_ITEM_TYPE } from '../../const/work-item-type';
import { BacklogTree, Idea, Task, Sprint } from '../../core/api';
import { Nullable } from '../../core/types/nullable';
import { createDataSource } from '../../core/utils/data-source-utils';
import { RootStore } from '../../stores';

export class BacklogStore {
  backlogSource: DataSource;
  sprintSource: DataSource;

  @observable
  itemSelection: Nullable<any> = null;

  @observable
  visibleWorkDetailPanel = false;

  constructor(private root: RootStore) {
    this.backlogSource = createDataSource({ api: BacklogTree, keyField: 'node_id' });
    this.sprintSource = createDataSource({ api: Sprint });

    makeObservable(this);
  }

  @action
  setSelection(selection: any): void {
    this.itemSelection = selection;
  }

  @action
  openWorkItemForm(type: WORK_ITEM_TYPE, data?: any): void {
    this.root.openWorkItemForm(type, data);
  }

  @action
  async removeWorkItem(type: WORK_ITEM_TYPE, data: any): Promise<void> {
    const result = await confirm('Вы уверены, что хотите удалить эту запись?', 'Подтверждение');

    if (result) {
      const cardStore = this.root.getWorkItem(type);
      if (cardStore && cardStore.api.delete) {
        try {
          await cardStore.api.delete(data.tid);
        } catch (E) {
          notify((E as any).message, 'error');
        }
      }
    }
  }

  @action
  async setWorkItemToSprint(itemData: any, sprintID: number): Promise<void> {
    const type = itemData.leafType;

    try {
      if (type === WORK_ITEM_TYPE.IDEA) {
        await Idea.setToSprint(itemData.tid, sprintID);
      } else if (type === WORK_ITEM_TYPE.TASK) {
        await Task.setToSprint(itemData.tid, sprintID);
      }
    } catch (E) {
      notify((E as any).message, 'error');
    }
  }

  @action
  toggleWorkDetailPanel(): void {
    this.visibleWorkDetailPanel = !this.visibleWorkDetailPanel;
  }
}
