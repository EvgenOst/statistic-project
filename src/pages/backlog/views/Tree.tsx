import { DropDownButton, ProgressBar } from 'devextreme-react';
import { RowDragging } from 'devextreme-react/data-grid';
import { Button as ColumnButton, Column, TreeList } from 'devextreme-react/tree-list';
import { ValueChangedEvent } from 'devextreme/ui/check_box';
import { ItemClickEvent as DropDownItemClickEvent } from 'devextreme/ui/drop_down_button';
import { Item as ToolbarItem } from 'devextreme/ui/toolbar';
import {
  ColumnButtonClickEvent,
  SelectionChangedEvent,
  ToolbarPreparingEvent,
} from 'devextreme/ui/tree_list';
import { observer } from 'mobx-react-lite';
import React, { FC, useCallback, useContext } from 'react';
import { WORK_ITEM_TYPE } from '../../../const/work-item-type';
import { createDictionaryApi } from '../../../core/api';
import { createLookupCfg } from '../../../core/utils/data-source-utils';
import { BacklogContext } from '../BacklogPage';

////////////////////////////////////

const addDropDownOptions = {
  width: 100,
};

const addButtonItems = [
  { type: WORK_ITEM_TYPE.PROJECT, text: 'Проект' },
  { type: WORK_ITEM_TYPE.IDEA, text: 'Идея' },
  { type: WORK_ITEM_TYPE.TASK, text: 'Задача' },
];

const stateLookup = createLookupCfg(createDictionaryApi('States'));
const sprintLookup = createLookupCfg(createDictionaryApi('Sprints'));

const renderProgress = (data: any) => (
  <div className="progress-cell">
    <ProgressBar value={data.value} />
  </div>
);

///////////////////////////////////

interface ITreeProps {
  className?: string;
}

export const Tree: FC<ITreeProps> = observer((props) => {
  const { backlogStore } = useContext(BacklogContext);

  const onToggleWorkDetailPanel = useCallback(() => {
    backlogStore.toggleWorkDetailPanel();
  }, [backlogStore]);

  const onReleaseViewChange = useCallback<(e: ValueChangedEvent) => void>(
    (e) => {
      const source = backlogStore.backlogSource;
      source.loadOptions().extraParams = { ShowReleases: e.value };
      source.reload();
    },
    [backlogStore]
  );

  const onAddWorkItem = useCallback<(e: DropDownItemClickEvent) => void>(
    (e) => {
      backlogStore.openWorkItemForm(e.itemData.type as WORK_ITEM_TYPE);
    },
    [backlogStore]
  );

  const onEditWorkItem = useCallback<(e: ColumnButtonClickEvent) => void>(
    (e) => {
      const data = e.row?.node.data;
      if (data) {
        backlogStore.openWorkItemForm(data.leafType, data);
      }
    },
    [backlogStore]
  );

  const onRemoveWorkItem = useCallback<(e: ColumnButtonClickEvent) => void>(
    (e) => {
      const data = e.row?.node.data;
      if (data) {
        backlogStore.removeWorkItem(data.leafType, data).then(() => {
          e.component.refresh();
        });
      }
    },
    [backlogStore]
  );

  const onToolbarPreparing = useCallback<(e: ToolbarPreparingEvent) => void>(
    (e) => {
      const defaults = (e.toolbarOptions.items || []) as ToolbarItem[];
      const items = [
        {
          widget: 'dxDropDownButton',
          location: 'before',
          options: {
            icon: 'add',
            text: 'Добавить',
            hint: 'Добавить',
            stylingMode: 'contained',
            displayExpr: 'text',
            dropDownOptions: addDropDownOptions,
            items: addButtonItems,
            onItemClick: onAddWorkItem,
          },
        },
        {
          widget: 'dxButton',
          location: 'before',
          options: {
            text: 'Планировать спринт',
            type: 'default',
            stylingMode: 'contained',
            onClick: onToggleWorkDetailPanel,
          },
        },
        {
          widget: 'dxCheckBox',
          location: 'before',
          options: {
            text: 'Релизы',
            onValueChanged: onReleaseViewChange,
          },
        },
      ] as ToolbarItem[];

      e.toolbarOptions.items = items.concat(defaults);
    },
    [onAddWorkItem, onToggleWorkDetailPanel, onReleaseViewChange]
  );

  const onSelectionChanged = useCallback<(e: SelectionChangedEvent) => void>(
    (e) => {
      const selected = e.selectedRowsData;
      backlogStore.setSelection(selected[0]);
    },
    [backlogStore]
  );

  return (
    <TreeList
      dataSource={backlogStore.backlogSource}
      dataStructure={'plain'}
      parentIdExpr={'parentId'}
      hasItemsExpr={(data: any) => !data.leaf}
      allowColumnResizing={true}
      allowColumnReordering={true}
      showRowLines={true}
      showColumnLines={true}
      height={'100%'}
      className={props.className}
      columnChooser={{ enabled: true, allowSearch: true, mode: 'select' }}
      filterPanel={{ visible: true }}
      paging={{ enabled: true }}
      selection={{ mode: 'single' }}
      onToolbarPreparing={onToolbarPreparing}
      onSelectionChanged={onSelectionChanged}
    >
      <RowDragging
        group={'sprintTask'}
        allowDropInsideItem={false}
        allowReordering={false}
        showDragIcons={false}
      />
      <Column dataField={'tid'} caption={'ID'} alignment={'left'} />
      <Column dataField={'Name'} caption={'Наименование'} />
      <Column dataField={'leafType'} caption={'Тип'} />
      <Column
        dataField={'State_id'}
        caption={'Статус'}
        calculateDisplayValue={'name_State_id'}
        lookup={stateLookup}
      />
      <Column
        dataField={'Sprint_id'}
        caption={'Спринт'}
        calculateDisplayValue={'name_Sprint_id'}
        lookup={sprintLookup}
      />
      <Column dataField={'CreateDate'} caption={'Дата начала'} dataType={'datetime'} />
      <Column dataField={'DateFinish'} caption={'Дата завершения'} dataType={'datetime'} />
      <Column dataField={'EstimatedTime'} caption={'Оценочное время'} dataType={'number'} />
      <Column dataField={'CompletedTime'} caption={'Завершенное время'} dataType={'number'} />
      <Column
        dataField={'PercentCompletion'}
        caption={'Процент завершения'}
        cellRender={renderProgress}
      />
      <Column type={'buttons'}>
        <DropDownButton
          icon={'add'}
          displayExpr={'text'}
          dropDownOptions={addDropDownOptions}
          items={addButtonItems}
          onItemClick={onAddWorkItem}
        />
        <ColumnButton icon={'edit'} onClick={onEditWorkItem} />
        <ColumnButton icon={'trash'} onClick={onRemoveWorkItem} />
      </Column>
    </TreeList>
  );
});
