import { DataGrid } from 'devextreme-react';
import { Column } from 'devextreme-react/data-grid';
import React, { FC, useRef } from 'react';
import { createSprintPeoplesApi } from '../../../core/api';
import { createDataSourceCfg } from '../../../core/utils/data-source-utils';

interface IPeopleGridProps {
  data: any;
}

export const PeopleGrid: FC<IPeopleGridProps> = (props) => {
  const dataSourceCfg = useRef(
    createDataSourceCfg({ api: createSprintPeoplesApi(props.data.data.tid) })
  );
  console.log('PeopleGrid props', props);

  return (
    <>
      <div className={'panel-title'}>Сотрудники</div>
      <DataGrid dataSource={dataSourceCfg.current} showColumnHeaders={false} showRowLines={true}>
        <Column dataField={'Name'} />
        <Column dataField={'EstimatedTime'} />
      </DataGrid>
    </>
  );
};
