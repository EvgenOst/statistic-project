import DataGrid, { Column, MasterDetail, RowDragging } from 'devextreme-react/data-grid';
import { RowDraggingAddEvent } from 'devextreme/ui/data_grid';
import { observer } from 'mobx-react-lite';
import React, { FC, useCallback, useContext } from 'react';
import { BacklogContext } from '../BacklogPage';

interface ISprintGridProps {
  detailTemplate: React.ComponentType<any>;
}

export const SprintGrid: FC<ISprintGridProps> = observer((props) => {
  const { backlogStore } = useContext(BacklogContext);

  const dragAddHandler = useCallback<(e: RowDraggingAddEvent) => void>(
    ({ component, itemData, toIndex, dropInsideItem }) => {
      const targetRow = component.getVisibleRows()[toIndex];
      if (dropInsideItem && targetRow) {
        component.beginCustomLoading('Сохранение');
        backlogStore.setWorkItemToSprint(itemData, targetRow.data.tid).then(() => {
          component.endCustomLoading();
          component.refresh();
        });
      }
    },
    [backlogStore]
  );
  const dragStartHandler = useCallback((e) => (e.cancel = true), []);
  return (
    <>
      <div className={'panel-title'}>Планирование спринта</div>
      <DataGrid
        dataSource={backlogStore.sprintSource}
        className={'sprint-grid flex-1'}
        showColumnHeaders={false}
        showRowLines={true}
      >
        <RowDragging
          group={'sprintTask'}
          allowDropInsideItem={true}
          allowReordering={false}
          showDragIcons={false}
          onDragStart={dragStartHandler}
          onAdd={dragAddHandler}
        />
        <MasterDetail enabled={true} component={props.detailTemplate} />
        <Column dataField={'Name'} />
      </DataGrid>
    </>
  );
});
