import { observer } from 'mobx-react-lite';
import React, { FC, useContext } from 'react';
import { BacklogContext } from '../BacklogPage';
import { PeopleGrid } from './PeopleGrid';
import { SprintGrid } from './SprintGrid';

export const WorkDetail: FC = observer(() => {
  const { backlogStore } = useContext(BacklogContext);

  if (!backlogStore.visibleWorkDetailPanel) return null;

  return (
    <div className={'work-detail-container layout-vertical'}>
      <SprintGrid detailTemplate={PeopleGrid} />
    </div>
  );
});
