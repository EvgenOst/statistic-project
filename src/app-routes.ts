import { withNavigationWatcher } from './contexts/navigation';
import { HomePage, BacklogPage } from './pages';

const routes: { path: string; component: any }[] = [{ path: '/backlog', component: BacklogPage }];

export default routes.map((route) => {
  return {
    ...route,
    component: withNavigationWatcher(route.component),
  };
});
