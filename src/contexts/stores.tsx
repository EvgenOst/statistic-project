import { Context, createContext } from 'react';
import { RootStoreInstance, RootStore, AuthStore } from '../stores';

export interface IStoresContextValue {
  rootStore: RootStore;
  authStore: AuthStore;
}

export const StoresContext = createContext<IStoresContextValue | null>(
  null
) as Context<IStoresContextValue>;

export function initContextsValues(): IStoresContextValue {
  return {
    rootStore: RootStoreInstance,
    authStore: RootStoreInstance.authStore,
  };
}
