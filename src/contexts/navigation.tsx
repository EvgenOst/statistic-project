import React, { useState, createContext, useContext, useEffect } from 'react';

interface INavigationContext {
  navigationData?: any;
  setNavigationData?: any;
}

interface INavigationProvider {
  children?: any;
}

const NavigationContext = createContext<INavigationContext>({});
const useNavigation = (): INavigationContext => useContext(NavigationContext);

function NavigationProvider(props: INavigationProvider): JSX.Element {
  const [navigationData, setNavigationData] = useState({});

  return <NavigationContext.Provider value={{ navigationData, setNavigationData }} {...props} />;
}

// eslint-disable-next-line
function withNavigationWatcher(Component: any) {
  // eslint-disable-next-line
  return function NavigationWatcher(props: any) {
    const { path } = props.match;
    const { setNavigationData } = useNavigation();

    useEffect(() => {
      setNavigationData({ currentPath: path });
    }, [path, setNavigationData]);

    return React.createElement(Component, props);
  };
}

export { NavigationProvider, useNavigation, withNavigationWatcher };
