export const API_URL = '/back/api';
export const AUTH_URL = '/api/v1/Auth';
export const USER_INFO_URL = '/api/v1/UserInfo';
export const COMBO_URL = '/api/v1/combo';
export const TASK_URL = '/api/v1/tasks';
export const TASK_TREE_URL = '/api/v1/tasks/tree';
