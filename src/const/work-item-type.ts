export enum WORK_ITEM_TYPE {
  PROJECT = 'Project',
  IDEA = 'Idea',
  TASK = 'Task',
  BUG = 'Bug',
}
