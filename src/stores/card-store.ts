import notify from 'devextreme/ui/notify';
import { action, makeObservable, observable } from 'mobx';
import { IBaseApi } from '../core/api';

export class CardStore {
  @observable
  public visible = false;

  @observable
  public isLoading = false;

  @observable
  public data: any = null;

  constructor(public api: IBaseApi) {
    makeObservable<CardStore>(this);
  }

  @action
  public show(): void {
    this.visible = true;
  }

  @action
  public close: () => void = () => {
    this.data = null;
    this.visible = false;
  };

  @action
  public async load(id: number): Promise<void> {
    if (!this.api.getOne) return;

    this.data = (await this.api.getOne(id)).data?.[0];
  }

  @action
  public async submit(): Promise<any> {
    if (!this.api.post) return;

    return this.api.post(this.data);
  }

  @action
  public async loadAndShow(id: number): Promise<void> {
    try {
      this.show();
      await this.load(id);
    } catch (E) {
      notify((E as any).message, 'error');
    }
  }

  @action
  public async submitAndClose(): Promise<void> {
    try {
      await this.submit();
      this.close();
    } catch (E) {
      notify((E as any).message, 'error');
    }
  }
}
