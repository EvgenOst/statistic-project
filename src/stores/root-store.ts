import Cookies from 'universal-cookie/es6';
import { AuthStore, CardStore } from '.';
import { WORK_ITEM_TYPE } from '../const/work-item-type';
import { Idea, Project, Task } from '../core/api';

export class RootStore {
  authStore: AuthStore;
  projectCardStore: CardStore;
  ideaCardStore: CardStore;
  taskCardStore: CardStore;

  constructor() {
    this.authStore = new AuthStore(new Cookies());
    this.projectCardStore = new CardStore(Project);
    this.ideaCardStore = new CardStore(Idea);
    this.taskCardStore = new CardStore(Task);

    // makeObservable(this);
  }

  public openWorkItemForm(type: WORK_ITEM_TYPE, data?: any): void {
    const cardStore = this.getWorkItem(type);
    if (cardStore) {
      if (data) {
        cardStore.loadAndShow(data.tid);
      } else {
        cardStore.show();
      }
    }
  }

  public getWorkItem(type: WORK_ITEM_TYPE): CardStore | undefined {
    switch (type) {
      case WORK_ITEM_TYPE.PROJECT:
        return this.projectCardStore;
      case WORK_ITEM_TYPE.IDEA:
        return this.ideaCardStore;
      case WORK_ITEM_TYPE.TASK:
        return this.taskCardStore;
    }
  }
}

export const RootStoreInstance = new RootStore();
