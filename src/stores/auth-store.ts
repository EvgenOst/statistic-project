import notify from 'devextreme/ui/notify';
import { action, flow, makeObservable, observable } from 'mobx';
import Cookies from 'universal-cookie/es6';
import { Auth, IAuthResponse, IUserInfoResponse } from '../core/api';
import { ResponseError } from '../core/services/request';
import { defaultUser } from '../utils/default-user';

const TOKEN_NAME = 'SessionGUID';

export class AuthStore {
  @observable
  userData: any = null;

  @observable
  initialized = false;

  @observable
  authorized = false;

  constructor(private cookieService: Cookies) {
    makeObservable(this);
  }

  @flow
  *init(): Generator<Promise<unknown>> {
    const token = this.cookieService.get(TOKEN_NAME);

    if (token) {
      try {
        const response = (yield Auth.checkSession()) as IUserInfoResponse;
        const user = response.user_id ? defaultUser : null;
        this.userData = user;
        this.authorized = !!user;
      } catch (e) {
        this.cookieService.remove(TOKEN_NAME);
        notify('Auth Error ' + (e as ResponseError).message, 'error', 5000);
      }
    }
    this.initialized = true;
  }

  @action
  async signIn(login: string, password: string): Promise<boolean> {
    try {
      const response = (await Auth.signIn(login, password)) as IAuthResponse;

      this.cookieService.set(TOKEN_NAME, response.SessionGUID, { path: '/' });
      this.userData = defaultUser;
      this.authorized = true;
    } catch (e) {
      notify('Auth Error ' + (e as ResponseError).message, 'error', 5000);
    }

    return this.authorized;
  }

  @action
  signOut(): void {
    this.cookieService.remove(TOKEN_NAME);
    this.authorized = false;
  }
}
